﻿using DLTDataUploader.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLTDataUploader
{
    public static class GLOBAL
    {

        public static string INTERVAL = System.Configuration.ConfigurationManager.AppSettings["INTERVAL"];
        public static int VENDOR_ID = 44;

        public static string USERNAME = System.Configuration.ConfigurationManager.AppSettings["USERNAME"];
        public static string PASSWORD = System.Configuration.ConfigurationManager.AppSettings["PASSWORD"];
        public static int APP_ID = int.Parse(System.Configuration.ConfigurationManager.AppSettings["APP_ID"]);
        public static string USERS_GROUP = System.Configuration.ConfigurationManager.AppSettings["USERS_GROUP"];

        public static string SERVER_HOST = System.Configuration.ConfigurationManager.AppSettings["SERVER_HOST"];
        public static string GPSPOINT_UPLOADER_ENDPOINT = System.Configuration.ConfigurationManager.AppSettings["GPSPOINT_UPLOADER_ENDPOINT"];
        public static string GPSPOINT_BACKUP_UPLOADER_ENDPOINT = System.Configuration.ConfigurationManager.AppSettings["GPSPOINT_BACKUP_UPLOADER_ENDPOINT"];
        public static string AUTHORIZATION_BASE = System.Configuration.ConfigurationManager.AppSettings["AUTHORIZATION_BASE"];       

        public static string DEEP_LOGGING = System.Configuration.ConfigurationManager.AppSettings["DEEP_LOGGING"];

    }
}
