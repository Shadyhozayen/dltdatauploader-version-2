﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DLTDataUploader.ServiceReference1 {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://gpsgate.com/services/", ConfigurationName="ServiceReference1.GeofenceSoap")]
    public interface GeofenceSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://gpsgate.com/services/SaveGeofences", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(ViewBag))]
        System.Xml.XmlNode SaveGeofences(string strSessionID, int appId, DLTDataUploader.ServiceReference1.GeofenceViewBag[] geofences);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://gpsgate.com/services/SaveGeofences", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> SaveGeofencesAsync(string strSessionID, int appId, DLTDataUploader.ServiceReference1.GeofenceViewBag[] geofences);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://gpsgate.com/services/RemoveGeofences", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(ViewBag))]
        System.Xml.XmlNode RemoveGeofences(string strSessionID, int appId, int[] geofencesIDs);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://gpsgate.com/services/RemoveGeofences", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Xml.XmlNode> RemoveGeofencesAsync(string strSessionID, int appId, int[] geofencesIDs);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1586.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://gpsgate.com/services/")]
    public partial class GeofenceViewBag : ViewBag {
        
        private int idField;
        
        private string nameField;
        
        private ShapeView shapeField;
        
        private int[] tagsField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public int ID {
            get {
                return this.idField;
            }
            set {
                this.idField = value;
                this.RaisePropertyChanged("ID");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string Name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
                this.RaisePropertyChanged("Name");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public ShapeView Shape {
            get {
                return this.shapeField;
            }
            set {
                this.shapeField = value;
                this.RaisePropertyChanged("Shape");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order=3)]
        public int[] Tags {
            get {
                return this.tagsField;
            }
            set {
                this.tagsField = value;
                this.RaisePropertyChanged("Tags");
            }
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(CircleView))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PolygonView))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(RouteView))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1586.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://gpsgate.com/services/")]
    public abstract partial class ShapeView : ViewBag {
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ShapeView))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(CircleView))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PolygonView))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(RouteView))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(GeofenceViewBag))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1586.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://gpsgate.com/services/")]
    public abstract partial class ViewBag : object, System.ComponentModel.INotifyPropertyChanged {
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1586.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://gpsgate.com/services/")]
    public partial class PositionView : object, System.ComponentModel.INotifyPropertyChanged {
        
        private double longitudeField;
        
        private double latitudeField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public double Longitude {
            get {
                return this.longitudeField;
            }
            set {
                this.longitudeField = value;
                this.RaisePropertyChanged("Longitude");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public double Latitude {
            get {
                return this.latitudeField;
            }
            set {
                this.latitudeField = value;
                this.RaisePropertyChanged("Latitude");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1586.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://gpsgate.com/services/")]
    public partial class CircleView : ShapeView {
        
        private PositionView centerField;
        
        private double radiusField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public PositionView Center {
            get {
                return this.centerField;
            }
            set {
                this.centerField = value;
                this.RaisePropertyChanged("Center");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public double Radius {
            get {
                return this.radiusField;
            }
            set {
                this.radiusField = value;
                this.RaisePropertyChanged("Radius");
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1586.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://gpsgate.com/services/")]
    public partial class PolygonView : ShapeView {
        
        private PositionView[] verticesField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order=0)]
        public PositionView[] Vertices {
            get {
                return this.verticesField;
            }
            set {
                this.verticesField = value;
                this.RaisePropertyChanged("Vertices");
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1586.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://gpsgate.com/services/")]
    public partial class RouteView : ShapeView {
        
        private PositionView[] verticesField;
        
        private double radiusField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order=0)]
        public PositionView[] Vertices {
            get {
                return this.verticesField;
            }
            set {
                this.verticesField = value;
                this.RaisePropertyChanged("Vertices");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public double Radius {
            get {
                return this.radiusField;
            }
            set {
                this.radiusField = value;
                this.RaisePropertyChanged("Radius");
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface GeofenceSoapChannel : DLTDataUploader.ServiceReference1.GeofenceSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class GeofenceSoapClient : System.ServiceModel.ClientBase<DLTDataUploader.ServiceReference1.GeofenceSoap>, DLTDataUploader.ServiceReference1.GeofenceSoap {
        
        public GeofenceSoapClient() {
        }
        
        public GeofenceSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public GeofenceSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public GeofenceSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public GeofenceSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public System.Xml.XmlNode SaveGeofences(string strSessionID, int appId, DLTDataUploader.ServiceReference1.GeofenceViewBag[] geofences) {
            return base.Channel.SaveGeofences(strSessionID, appId, geofences);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> SaveGeofencesAsync(string strSessionID, int appId, DLTDataUploader.ServiceReference1.GeofenceViewBag[] geofences) {
            return base.Channel.SaveGeofencesAsync(strSessionID, appId, geofences);
        }
        
        public System.Xml.XmlNode RemoveGeofences(string strSessionID, int appId, int[] geofencesIDs) {
            return base.Channel.RemoveGeofences(strSessionID, appId, geofencesIDs);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> RemoveGeofencesAsync(string strSessionID, int appId, int[] geofencesIDs) {
            return base.Channel.RemoveGeofencesAsync(strSessionID, appId, geofencesIDs);
        }
    }
}
