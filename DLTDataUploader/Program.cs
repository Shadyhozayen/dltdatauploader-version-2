﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DLTDataUploader
{
    static class Program
    {
        static Mutex m;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool first = false;
            log4net.Config.XmlConfigurator.Configure();

            m = new Mutex(true, Application.ProductName.ToString(), out first);
            if ((first))
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                AppTimer appTimer = new AppTimer();
                Application.Run(new AppContext());
            }
            else
            {
                MessageBox.Show("Application" + " " + Application.ProductName.ToString() + " " + "already running");
            }
        }
    }
}
