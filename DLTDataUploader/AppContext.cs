﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DLTDataUploader
{
    public class AppContext : ApplicationContext
    {
        ServerConfiguration configWindow = new ServerConfiguration();
        public static NotifyIcon notifyIcon = new NotifyIcon();
        public AppContext()
        {
            MenuItem configMenuItem = new MenuItem("Configuration", new EventHandler(ShowConfig));
            MenuItem exitMenuItem = new MenuItem("Exit", new EventHandler(Exit));


            notifyIcon.Icon = Properties.Resources.GPS;
            notifyIcon.ContextMenu = new ContextMenu(new MenuItem[]
                { configMenuItem, exitMenuItem });
            notifyIcon.Visible = true;
        }



        void ShowConfig(object sender, EventArgs e)
        {
            // If we are already showing the window, merely focus it.
            if (configWindow.Visible)
            {
                configWindow.Activate();
            }
            else
            {
                configWindow.ShowDialog();
            }
        }

        void Exit(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure to exit to exit the appliction ?", "Exit Applcation", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                notifyIcon.Visible = false;
                Application.Exit();
            }
        }
    }
}
