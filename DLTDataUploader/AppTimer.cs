﻿using DLTDataUploader.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Timers;
using System.Linq;

namespace DLTDataUploader
{
    public class AppTimer
    {
        DataServices dataServices = new DataServices();
        public AppTimer()
        {
            try
            {
                int interval = int.Parse(GLOBAL.INTERVAL) * 1000; // Seconds to milliseconds
                while (true)
                {
                    dataServices.FetchData();
                    Thread.Sleep(interval);
                }
            }
            catch (Exception ex)
            {
                logger.log.Error("AppTimer : " + ex.Message);
            }
        }






    }
}
