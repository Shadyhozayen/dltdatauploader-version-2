﻿using DLTDataUploader.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace DLTDataUploader
{
    public class DataServices
    {
        DirectoryService.DirectorySoapClient dirService;
        string sessionId = "";

        public DataServices()
        {
            dirService = new DirectoryService.DirectorySoapClient();
        }

        public void FetchData()
        {
            try
            {
                //Get Users
                Devices usersDevices = GetUsersInGroup(GLOBAL.USERS_GROUP);

                //Filter users with updated points ONLY + update DB with updated positions
                Devices filteredDevices = FilterPositions(usersDevices);

                List<DLTGPSPoint> lstGPSPoints = GetPoints(filteredDevices);
                if (lstGPSPoints.Count > 0)
                {
                    logger.log.Info("Processing " + lstGPSPoints.Count + " points");
                    bool sent = UploadGPSPoints(lstGPSPoints);
                }
            }
            catch (Exception ex)
            {
                logger.log.Error("Exception", ex);
            }
        }

        public List<DLTGPSPoint> GetPoints(Devices devices)
        {
            List<DLTGPSPoint> points = new List<DLTGPSPoint>();
            try
            {
                var context = new VehiclesDbContext();
                var vehicles = context.Vehicles.ToList();
                foreach (var usr in devices.users)
                {
                    #region filling data

                    DLTGPSPoint dltGpsPoint = new DLTGPSPoint();
                    string rfid = "";
                    int userID = usr.id;
                    var qry = vehicles.Where(c => c.VehicleID == userID.ToString()).FirstOrDefault();
                    rfid = (qry.RFID != null ? qry.RFID : "");

                    dltGpsPoint.DriverID = rfid.Replace(" ", "");
                    dltGpsPoint.UnitID = usr.vehicle.UnitID;
                    dltGpsPoint.UTCTs = usr.trackPoint.utc.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
                    dltGpsPoint.RecvUtcTs = usr.trackPoint.utc.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
                    dltGpsPoint.Lat = usr.trackPoint.pos.lat;
                    dltGpsPoint.Lon = usr.trackPoint.pos.lng;
                    dltGpsPoint.Alt = (int)usr.trackPoint.pos.alt;
                    dltGpsPoint.Speed = (int)(usr.trackPoint.vel.speed * 3.6);
                    dltGpsPoint.Course = int.Parse(usr.trackPoint.vel.heading.ToString());
                    dltGpsPoint.MaxSpeed = dltGpsPoint.Speed;
                    dltGpsPoint.EngineStatus = (qry.Ignition == "1" ? 1 : 0);
                    points.Add(dltGpsPoint);
                    #endregion
                }

            }
            catch (Exception ex)
            {
                logger.log.Error("GetPoints : " + ex.Message);
            }
            return points;
        }

        private bool Connect()
        {
            try
            {
                sessionId = "";
                XmlNode xmlResponse = dirService.Login(GLOBAL.USERNAME, GLOBAL.PASSWORD, GLOBAL.APP_ID);
                if (xmlResponse.Name == "sessionId")
                {
                    sessionId = ((XmlElement)xmlResponse).InnerText;
                    logger.log.Info("Connected with session id : " + sessionId);
                    return true;
                }
                else
                {
                    logger.log.Info("Cannot connect!");
                    logger.log.Error(((XmlElement)xmlResponse).InnerText);
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.log.Error("Connect() : " + ex.Message);
                return false;
            }
        }

        public Devices GetUsersInGroup(string userGroup)
        {
            Devices usersDevices = new Devices();
            users usersList = new users();
            try
            {
                var lstUsers = dirService.GetUsersInUserTag(sessionId, GLOBAL.APP_ID, userGroup);
                if (lstUsers.Name == "error")
                {
                    Connect();
                    lstUsers = dirService.GetUsersInUserTag(sessionId, GLOBAL.APP_ID, userGroup);
                }
                usersList = GetData(lstUsers.OuterXml, new users());

                foreach (var u in usersList.user)
                {

                    string[] vehicleParameters = u.description.Split('|');
                    var veh = new DLTVehicle()
                    {
                        UserID = u.id,
                        CardReader = 1,
                        ProvinceCode = (vehicleParameters.Length > 4 ? vehicleParameters[4] : ""),
                        UnitID = (vehicleParameters.Length > 1 ? vehicleParameters[1] : ""),
                        VehicleChassisNo = (vehicleParameters.Length > 0 ? vehicleParameters[0] : ""),
                        VehicleID = u.name,
                        VehicleRegisterType = (vehicleParameters.Length > 3 ? vehicleParameters[3] : ""),
                        VehicleType = (vehicleParameters.Length > 2 ? vehicleParameters[2] : ""),
                        VendorID = GLOBAL.VENDOR_ID
                    };
                    u.vehicle = veh;
                }
                usersDevices.users = new List<user>(usersList.user);
            }
            catch (Exception ex)
            {
                logger.log.Error("GetUsersInGroup : " + ex.Message);
            }
            return usersDevices;
        }

        private dynamic GetData(string data, dynamic type)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(type.GetType());
                using (TextReader rd = new StringReader(data))
                {
                    var result = serializer.Deserialize(rd);
                    return result;
                }
            }
            catch (Exception ex)
            {
                logger.log.Error("GetData : " + ex.Message);
                return null;
            }
        }

        public bool UploadGPSPoints(List<DLTGPSPoint> lstGPSPoints)
        {
            try
            {
                DLTPacket dltPacket = new DLTPacket();
                dltPacket.VendorID = 44;
                dltPacket.LocationCount = lstGPSPoints.Count;
                dltPacket.GPSPoints = lstGPSPoints.ToArray();
                string json = JsonConvert.SerializeObject(dltPacket, Newtonsoft.Json.Formatting.None);

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(GLOBAL.SERVER_HOST + GLOBAL.GPSPOINT_UPLOADER_ENDPOINT);
                httpWebRequest.ContentType = "application/json;charset=utf-8";
                httpWebRequest.Method = "POST";
                // httpWebRequest.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => { return true; };
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(GLOBAL.AUTHORIZATION_BASE);
                var authorizationKey = System.Convert.ToBase64String(plainTextBytes);
                httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "Basic " + authorizationKey);

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    string result = streamReader.ReadToEnd();
                    logger.log.Info(result);
                }

                if (GLOBAL.DEEP_LOGGING == "1")
                    logger.log.Info(json);

                return true;
            }
            catch (Exception ex)
            {
                logger.log.Error("UploadGPSPoints : " + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Filter devices with updated positions ONLY & Update DB with SOAP data
        /// </summary>
        /// <param name="devices"></param>
        /// <returns></returns>
        public Devices FilterPositions(Devices devices)
        {
            Devices updatedDevices = new Devices() { users = new List<user>() };
            try
            {
                using (var context = new VehiclesDbContext())
                {
                    var vehicles = context.Vehicles.ToList();
                    foreach (var usr in devices.users)
                    {

                        var histquery = vehicles.Where(c => c.VehicleID == usr.id.ToString()
                        && c.UpdatedUTC != usr.trackPoint.utc.ToString("yyyy-MM-ddTHH:mm:ss")
                        ).ToList();

                        if (histquery.Count() > 0)
                        {
                            var data = histquery.FirstOrDefault();
                            data.UpdatedUTC = usr.trackPoint.utc.ToString("yyyy-MM-ddTHH:mm:ss");
                            data.Latitude = usr.trackPoint.pos.lat.ToString();
                            data.Longitude = usr.trackPoint.pos.lng.ToString();

                            updatedDevices.users.Add(usr);
                        }
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.log.Error("FilterPositions : " + ex.Message);
            }
            return updatedDevices;
        }

        //public void UpdateHistory(List<DLTGPSPoint> points)
        //{
        //    try
        //    {
        //        using (var context = new VehiclesDbContext())
        //        {
        //            var vehiclesList = context.Vehicles.ToList();
        //            foreach (var point in points)
        //            {
        //                var qry = vehiclesList.Where(c => c.VehicleID == point.UserID.ToString()).ToList();
        //                //Update current vehicle last position
        //                if (qry.Count() > 0)
        //                {
        //                    var data = qry.FirstOrDefault();
        //                    data.Longitude = point.Lon.ToString();
        //                    data.Latitude = point.Lat.ToString();
        //                    data.UpdatedUTC = point.UTCUpdatedTime.ToString("yyyy-MM-ddTHH:mm:ss");
        //                }
        //                else
        //                {
        //                    //If no vehicle => add vehicle with latest position
        //                    context.Vehicles.Add(new Vehicle()
        //                    {
        //                        VehicleID = point.UserID.ToString(),
        //                        Longitude = point.Lon.ToString(),
        //                        Latitude = point.Lat.ToString(),
        //                        UpdatedUTC = point.UTCUpdatedTime.ToString("yyyy-MM-ddTHH:mm:ss")
        //                    });
        //                }
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        logger.log.Error("UpdateHistory : " + ex.Message);
        //    }
        //}

    }
}

