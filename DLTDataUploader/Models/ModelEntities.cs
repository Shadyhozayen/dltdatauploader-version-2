﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DLTDataUploader.Models
{
    public class TrackRecord
    {
        [XmlElement("trackInfo")]
        public trackInfo trackInfo { get; set; }
        [XmlElement("trackPoints")]
        public trackPoints trackPoints { get; set; }
    }
    [XmlRoot("tracks")]
    public class tracks
    {
        [XmlElement("trackInfo")]
        public List<trackInfo> trackInfo { get; set; }
    }

    public class trackInfo
    {
        [XmlElement("id")]
        public int id { get; set; }
        [XmlElement("ownerId")]
        public int ownerId { get; set; }
        [XmlElement("name")]
        public string name { get; set; }
    }

    [XmlRoot("trackPoints")]
    public class trackPoints
    {
        [XmlElement("trackPoint")]
        public List<trackPoint> trackPoint { get; set; }
    }


    public class trackPoint
    {
        [XmlElement("pos")]
        public pos pos { get; set; }
        [XmlElement("vel")]
        public vel vel { get; set; }
        [XmlElement("utc")]
        public DateTime utc { get; set; }
        //public string address { get; set; }
        [XmlElement("valid")]
        public bool valid { get; set; }
    }

    [XmlRoot("user")]
    public class user
    {
        [XmlElement("id")]
        public int id { get; set; }
        [XmlElement("username")]
        public string username { get; set; }
        [XmlElement("name")]
        public string name { get; set; }
        [XmlElement("description")]
        public string description { get; set; }
        [XmlElement("trackPoint")]
        public trackPoint trackPoint { get; set; }

        [XmlArray("attributes")]
        [XmlArrayItem("attribute", typeof(Attribute))]
        public List<Attribute> Attributes { get; set; }

        public DLTVehicle vehicle { get; set; }
        //[XmlElement("attributes")]
        //public List<attribute> attributes { get; set; }
    }

    [XmlRoot("attribute")]
    public class Attribute
    {
        [XmlElement("name")]
        public string Name { get; set; }
        [XmlElement("type")]
        public string Type { get; set; }
        [XmlElement("value")]
        public string Value { get; set; }
    }
    //[XmlRoot("attribute")]
    //public class attribute
    //{
    //    [XmlElement("value")]
    //    public string value { get; set; }
    //    [XmlElement("type")]
    //    public string type { get; set; }
    //    [XmlElement("name")]
    //    public string name { get; set; }
    //}

    public class pos
    {
        [XmlElement("lng")]
        public double lng { get; set; }
        [XmlElement("lat")]
        public double lat { get; set; }
        [XmlElement("alt")]
        public double alt { get; set; }
    }
    public class vel
    {
        [XmlElement("speed")]
        public float speed { get; set; }
        [XmlElement("heading")]
        public float heading { get; set; }
    }

    public class SimpleTrack
    {
        public string name { get; set; }
        public decimal longitude { get; set; }
        public decimal latitude { get; set; }
        public decimal altitude { get; set; }
        public float speed { get; set; }
        public float heading { get; set; }
        public DateTime time { get; set; }

    }
    [XmlRoot("result")]
    public class users
    {
        [XmlElement("user")]
        public List<user> user { get; set; }
    }
    public class reverseGeocodeResult
    {
        public List<loc> locations { get; set; }
    }
    public class loc
    {
        public int id { get; set; }
        public pos pos { get; set; }
        public string address { get; set; }
        public string formattedResult { get; set; }
    }

    public class DLTVehicle
    {
        public int VendorID { get; set; }
        public int UserID { get; set; }
        public string UnitID { get; set; }
        public string VehicleID { get; set; }
        public string VehicleType { get; set; }
        public string VehicleChassisNo { get; set; }
        public string VehicleRegisterType { get; set; }
        public int CardReader { get; set; }
        public string ProvinceCode { get; set; }
        
    }


    public class DLTGPSPoint
    {
        [XmlElement("driver_id")]
        [JsonProperty("driver_id")]
        public string DriverID { get; set; }

        [XmlElement("unit_id")]
        [JsonProperty("unit_id")]
        public string UnitID { get; set; }

        [XmlElement("seq")]
        [JsonProperty("seq")]
        public int Seq { get { return 0; } }

        [XmlElement("utc_ts")]
        [JsonProperty("utc_ts")]
        public string UTCTs { get; set; }

        [XmlElement("recv_utc_ts")]
        [JsonProperty("recv_utc_ts")]
        public string RecvUtcTs { get; set; }

        [XmlElement("lat")]
        [JsonProperty("lat")]
        public double Lat { get; set; }

        [XmlElement("lon")]
        [JsonProperty("lon")]
        public double Lon { get; set; }

        [XmlElement("alt")]
        [JsonProperty("alt")]
        public int Alt { get; set; }

        [XmlElement("speed")]
        [JsonProperty("speed")]
        public int Speed { get; set; }

        [XmlElement("engine_status")]
        [JsonProperty("engine_status")]
        public int EngineStatus { get; set; }

        [XmlElement("fix")]
        [JsonProperty("fix")]
        public int Fix { get { return 1; } }

        [XmlElement("license")]
        [JsonProperty("license")]
        public string License { get; set; }

        [XmlElement("course")]
        [JsonProperty("course")]
        public int Course { get; set; }

        [XmlElement("hdop")]
        [JsonProperty("hdop")]
        public int HDOP { get { return 0; } }

        [XmlElement("num_sats")]
        [JsonProperty("num_sats")]
        public int NumSats { get { return 0; } }

        [XmlElement("gsm_cell")]
        [JsonProperty("gsm_cell")]
        public int GSMCell { get { return 0; } }

        [XmlElement("gsm_loc")]
        [JsonProperty("gsm_loc")]
        public int GSMLoc { get { return 0; } }

        [XmlElement("gsm_rssi")]
        [JsonProperty("gsm_rssi")]
        public int GSMRssi { get { return 0; } }

        [XmlElement("mileage")]
        [JsonProperty("mileage")]
        public int Mileage { get { return 0; } }

        [XmlElement("ext_power_status")]
        [JsonProperty("ext_power_status")]
        public int ExtPowerStatus { get { return 0; } }

        [XmlElement("ext_power")]
        [JsonProperty("ext_power")]
        public int ExtPower { get { return 0; } }

        [XmlElement("high_acc_count")]
        [JsonProperty("high_acc_count")]
        public int? HighAccCount { get; set; }

        [XmlElement("high_de_acc_count")]
        [JsonProperty("high_de_acc_count")]
        public int? HighDeAccCount { get; set; }

        [XmlElement("over_speed_count")]
        [JsonProperty("over_speed_count")]
        public int? OverSpeedCount { get; set; }

        [XmlElement("max_speed")]
        [JsonProperty("max_speed")]
        public int MaxSpeed { get; set; }
    }

    public class DLTPacket
    {
        [JsonProperty(PropertyName = "vender_id")]
        [XmlElement("vender_id")]
        public int VendorID { get; set; }
        [JsonProperty(PropertyName = "locations_count")]
        [XmlElement("locations_count")]
        public int LocationCount { get; set; }
        [XmlElement("locations")]
        [JsonProperty(PropertyName = "locations")]
        public DLTGPSPoint[] GPSPoints { get; set; }
    }

    public class Devices
    {
        //public List<DLTVehicle> vehicles { get; set; }
        public List<user> users { get; set; }
        //public List<DevicePosition> devicesPositions { get; set; }
    }
    public class DevicePosition
    {
        public int UserId { get; set; }
        public DateTime UpdatedTime { get; set; }
    }

    public class ServerResponse
    {
        public int code { get; set; }
        public string message { get; set; }
        public int received_records { get; set; }
    }

    public class Vehicle
    {
        public int ID { get; set; }
        public string VehicleID { get; set; }
        public string Ignition { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string RFID { get; set; }
        public string UpdatedUTC { get; set; }
    }
    public class VehicleHistory
    {
        public int ID { get; set; }
        public string VehicleID { get; set; }
        public string Event { get; set; }
        public string Comment { get; set; }
        public string UpdatedUTC { get; set; }
    }
}
