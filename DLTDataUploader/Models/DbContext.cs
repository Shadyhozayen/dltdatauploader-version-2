﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using DLTDataUploader.Models;

namespace DLTDataUploader
{
    public class VehiclesDbContext : DbContext
    {
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<VehicleHistory> VehiclesHistory { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Chinook Database does not pluralize table names
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //            Database.SetInitializer<VehiclesDbContext>(
            //new MigrateDatabaseToLatestVersion<VehiclesDbContext, , System.Data.Entity.Migrations.DbMigration>(
            //    useSuppliedContext: true));
        }


    }
}
